class ValidaForm
{
	constructor()
	{
		this.btn_add	= document.querySelector("#adicionar_dados");
		this.data		= new Date();
		this.ano_atual	= this.data.getFullYear();
	}



	valida_data()
	{
		var anoInformado	= document.querySelector("input[name='ano']");
		var anoMinimoValido = 1900;


		if( anoInformado >= anoMinimoValido && anoInformado <= this.ano_atual )
		{
			console.log('Ano válido!');
		}
		else if( anoInformado == "" )
		{
			alert('Preecha a data!');
		}
		else
		{
			alert('Ano invalido!');
		}
	}
}


var post = new ValidaForm();
post.btn_add.addEventListener('click', post.valida_data);
