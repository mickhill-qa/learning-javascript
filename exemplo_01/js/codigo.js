// Variaveis Globais
var campo_nome		= document.querySelector("input[name='nome']");
var campo_data		= document.querySelector("input[name='ano']");
var btn_add			= document.querySelector("#adicionar_dados");

var data			= new Date();
var ano_atual		= data.getFullYear();
var entrevistas		= new Array();







// Objetos
class Pessoa
{
	constructor(in_nome, in_ano_nasc)
	{
		this.nome 		= in_nome;
		this.ano_nasc	= in_ano_nasc;
	}

	calc_idade()
	{
		return ano_atual - this.ano_nasc;
	}
}







// Funções
function valida_nome(in_nome)
{
	var etiquetaCampo	= document.querySelector("label[for='nome']").textContent;

	if (in_nome != "")
	{
		return true;
	}
	else
	{
		swal({
			title: 'Aviso!',
			text:  'Preecha o campo: ' + etiquetaCampo + '.',
			type:  'warning'
		});
		return false;
	}
}

function valida_ano(in_data)
{
	var anoInformado	= in_data;
	var idadeMaxima		= 115;
	var anoMinimoValido = ano_atual - idadeMaxima;
	var etiquetaCampo	= document.querySelector("label[for='ano']").textContent;


	if( anoInformado >= anoMinimoValido && anoInformado <= ano_atual )
	{
		return true;
	}
	else if( anoInformado == "" )
	{
		swal({
			title: 'Aviso!',
			text:  'Preecha o campo: ' + etiquetaCampo + '.',
			type:  'warning'
		});
	}
	else
	{
		if ( anoInformado < anoMinimoValido )
		{
			swal({
				title: 'Erro!',
				text:  'O ano minimo permitido é: ' + anoMinimoValido,
				type:  'error'
			});
		}
		else
		{
			swal({
				title: 'Erro!',
				text:  'O ano maximo permitido é: ' + ano_atual + '.',
				type:  'error'
			});s
		}
		return false;
	}
}







// Rotina
btn_add.addEventListener(
	'click',
	function(event)
	{
		event.preventDefault();

		if (
			valida_nome(campo_nome.value.trim()) &&
			valida_ano(campo_data.value.trim())
		){
			var pe = new Pessoa(campo_nome.value.trim(), campo_data.value.trim());

			swal(
				{
					title: "Adionar?",
					text: 'Deseja adicionar '+ pe.nome +' de '+ pe.calc_idade() +' ano(s) a lista?',
					type: "info",
					showCancelButton: true,
					confirmButtonText: "Confirma",
					closeOnConfirm: false
				},
				function()
				{
					swal("Adicionado!", "Entrevista adicionada com sucesso.", "success");
					entrevistas.push(pe);
					console.log(pe);
				}
			);
		}
	}
);