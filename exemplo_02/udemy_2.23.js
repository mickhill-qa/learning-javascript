var criaPessoa = function(nome){
    var _nome = nome;

    return {
        getNome: function(){
            return _nome;
        },
        setNome: function(novoNome){
            return _nome = novoNome;
        }
    };
};

var jose = new criaPessoa();
var maria = criaPessoa('Maria');

console.log(jose.getNome());
console.log(maria.getNome());