var Pessoa = function(nome){
    this.nome = nome;
}

var joao  = new Pessoa("João");
var maria = new Pessoa("Maria");

console.log(Pessoa);
Pessoa.prototype.idade = 10;


console.log(joao);
console.log(maria);
console.log(Pessoa);